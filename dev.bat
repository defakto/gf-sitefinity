@echo off

set site=globalfoundries.dev
set ip=127.0.0.1
set db=.\sql2012express
set sandbox="F:\dev\globalfoundries\sandbox"
set port=27109

set curdir=%cd%
set devmode=%1
set hostsfile=%windir%\System32\drivers\etc\hosts
SET appcmd=CALL %WINDIR%\system32\inetsrv\appcmd
SET NEWLINE=^& echo.


echo %devmode% and %curdir% x

If "%devmode%"=="attach" goto :attach
If "%devmode%"=="detach" goto :detach
If "%devmode%"=="" goto :help

goto :enddev

:attach
echo Attaching %site% to development environment...

FIND /c "%site%" %hostsfile%
IF %ERRORLEVEL% NEQ 0 (
	ECHO %NEWLINE%^%ip% %site%>>%hostsfile%
)

%appcmd% list site /name:"%site%"
IF %ERRORLEVEL% NEQ 0 (
	%appcmd% add apppool /name:%site% /managedRuntimeVersion:v4.0
	%appcmd% add site /name:"%site%" /physicalPath:%sandbox% /bindings:http/%ip%:80:%site%
	%appcmd% set app "%site%/" /applicationPool:"%site%"
)

sqlcmd -E -S %db% -v devmode="'%devmode%'" -v datapath="'%curdir%\..\data\'" -i %curdir%\..\data\dev.sql

goto :enddev

:detach
echo Removing %site from development environment...

FIND /c "%site%" %hostsfile%
IF %ERRORLEVEL% NEQ 1 (
	find /v "%ip% %site%" < %hostsfile% >%hostsfile%tmp 
	copy /a /y %hostsfile%tmp %hostsfile%
)
%appcmd% list site /name:"%site%"
IF %ERRORLEVEL% EQU 0 (
	%appcmd% delete site "%site%"
	%appcmd% delete apppool "%site%"
)

sqlcmd -E -S %db% -v devmode="'%devmode%'" -v datapath="'%curdir%\..\data\'" -i %curdir%\..\data\dev.sql


goto :enddev

:help
	echo Usage: 
	echo dev.bat attach (configures %site% project.) 
	echo dev.bat detach (removes %site% configuration.) 

goto :enddev


:enddev
