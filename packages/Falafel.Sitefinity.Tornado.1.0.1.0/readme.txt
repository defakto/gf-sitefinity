===================================================
Name: Falafel Tornado for Sitefinity
Written by: Falafel Software Inc.
Website: http://www.falafel.com
===================================================

Thank you for downloading Falafel Tornado!
Below is a quick video to get you started:
http://www.youtube.com/watch?v=bzXR269CgI0

------------------------------------------
Running Environment:
------------------------------------------
Falafel Tornado has been fully tested on
  -Sitefinity Version 5.4+
  -IIS7+

------------------------------------------
Installation:
------------------------------------------
1) Build your SitefinityWebApp after installing the NuGet package
2) Import the new module located at "~/App_Data/Sitefinity/Temp/Widget blocks.zip" by going to:
	Administration > Module Builder > Import a module
3) Click "Activate this module"
4) That's it!

Note: Be sure to update the assembly redirections in your web.config to the
	correct Sitefinity version at configuration > runtime > assemblyBinding

------------------------------------------
Usage:
------------------------------------------
For a plain jQuery widget:
1) Go to Content > Widget blocks > Create a widget block and enter:
	Title: Test 123
	Scripts: $('body').prepend('<h1>Added by Falafel Tornado!</h1>');
	Include jQuery: Yes
2) Go to Pages and edit any page
3) Drag and drop "Widget block" from the page toolbox under "Content"
4) Click "Edit" on the new widget and click "Change" to choose "Test 123"
5) That's it!

For downloading widgets from the marketplace:
1) Go to Content > Widget blocks > Marketplace...
2) That's it! Have fun!!

Note: You may edit various configurations from the administration:
	Administration > Settings > Advanced > Falafel

Note: Include stylesheets in your page templates if needed, such as KendoUI and Twitter Bootstrap:
	<link href="~/Async/libs/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="~/Async/libs/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
	<link href="~/Async/libs/kendoui/css/kendo.common.min.css" rel="stylesheet" type="text/css" />
	<link href="~/Async/libs/kendoui/css/kendo.bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="~/Async/libs/kendoui/css/kendo.dataviz.min.css" rel="stylesheet" type="text/css" />

Note: A vast collection of Web API services are included. For a JavaScript wrapper, see here:
	https://github.com/FalafelSoftwareInc/Falafel.Async/blob/master/Async/libs/sitefinity/js/api.js

Note: You can override and extend the web services in the "~/Api" folder

Note: Optionally add the following custom fields for more data in the web services:
	Images Modules > Short Text field called "Link"
	News, Events, and Posts Modules > Short Text field called "Images" with custom control of
		"Telerik.Sitefinity.Web.UI.Fields.ImageField" (non-localizable)

Note: To initialize your JavaScript app or extend the RequireJS configurations, 
configure a startup script at Administration > Settings > Advanced > Falafel > Startup Scripts

Note: For more details of the scripts libraries and RequireJS configurations, see the docs of
the dependency called Falafel.Async: https://github.com/FalafelSoftwareInc/Falafel.Async

------------------------------------------
Sitefinity Training:
------------------------------------------
We offer step by step instructor-led training online and onsite to train
your team on using, configuring , optimizing and developing for Sitefinity 5. For
classes and schedule, please visit: http://www.falafel.com/consulting/sitefinity-services

------------------------------------------
Custom Sitefinity Solutions:
------------------------------------------
Falafel can make your Sitefinity experience perfect. 
Whether you need help installing Sitefinity for the first time, 
integrating with another system or a custom module, Falafel can help. 
Contact us at info@falafel.com to discuss your project.


ENJOY!!