===================================================
Name: Sitefinity 5+ Code Explorer
Written by: Falafel Software Inc.
Website: http://falafel.com
===================================================

------------------------------------------
Running Environment:
------------------------------------------
The Code Explorer has been fully tested on
  -Sitefinity 5+
  -ASP.NET 4+
  -IIS7+

------------------------------------------
Installation:
------------------------------------------
1) Register the widget to your toolbox by going to:
	Administration > Settings > Advance > Toolboxes > Toolboxes > PageControls > Section
2) Click "Create new" and enter:
	Name = Admin
	Title = Admin
3) Click on Admin > Tools
4) Click "Create new" and enter:
	ControlType = ~/Custom/CodeExplorer/FileManager.ascx
	Name = CodeExplorer
	Title = Code Explorer
5) Next create the admin page by going to Administration > Backend Pages
	Note: Accept the warning
6) Put a checkmark by the "Sitefinity > Design" group and create a child page
7) Enter the following page properties:
	Name = Code Explorer
8) Construct the page:
	Drag the widget under "Admin" called "Code Explorer" (edit the properties if you like)
9) Click "Publish" and a new "Code Explorer" menu item appear under "Design"

------------------------------------------
Usage:
------------------------------------------
1) Log into admin and go to Design > Code Explorer
2) Locate the file you would like to edit
3) Double-click to open the code editor
4) Click the save icon to publish your changes

------------------------------------------
Sitefinity Training:
------------------------------------------
We offer step by step instructor-led training online and onsite to train
your team on using, configuring , optimizing and developing for Sitefinity 5. For
classes and schedule, please visit: http://www.falafel.com/consulting/sitefinity-services

------------------------------------------
Custom Sitefinity Solutions:
------------------------------------------
Falafel can make your Sitefinity experience perfect. 
Whether you need help installing Sitefinity for the first time, 
integrating with another system or a custom module, Falafel can help. 
Contact us at info@falafel.com to discuss your project.


ENJOY!!