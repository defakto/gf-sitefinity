﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Sitefinity;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Modules.Pages;
using Telerik.Sitefinity.Web;
using System.Globalization;
using Telerik.Sitefinity.Taxonomies.Model;
using Telerik.Sitefinity.Taxonomies;
using Telerik.OpenAccess;
using Telerik.Sitefinity.News.Model;
using System.Reflection;
using Telerik.Sitefinity.Modules.News;

namespace SitefinityWebApp.Widgets
{
    public class NewsExtension : NewsItem
    {
        private string realUrl;

        public NewsExtension() { }

        public NewsExtension(NewsItem item) 
        {
            if (item != null)
                MapFromNewsItem(item);
        }

        public string StringUrl 
        {
            get { return realUrl;}
            set { realUrl = value; }
        }

        private void MapFromNewsItem(NewsItem item)
        {
            PropertyInfo[] basePIs = typeof(NewsItem).GetProperties();
            Type thisObjectType = this.GetType();

            foreach (PropertyInfo basePI in basePIs)
            {
                PropertyInfo newPI = thisObjectType.GetProperty(basePI.Name);
                if (newPI != null)
                {
                    try
                    {
                        if (newPI.CanWrite && newPI != null)
                        {
                            newPI.SetValue(this, basePI.GetValue(item, null), null);
                        }

                    }
                    catch { }
                }
            }
        }
    }

    public partial class PressReleaseSidebarList : System.Web.UI.UserControl
    {
        private int count;
        private string samplenewsitem;
        private string categoryname;
        private Guid PageId;

        ///<summary> 
        /// This property is for the number of items to return
        ///</summary>
        public int Count
        {
            get { return count; }
            set { count = value; }
        }

        /// <summary>
        ///  This property is for the title of a sample news item
        /// </summary>
        public string SampleNewsItem
        {
            get { return samplenewsitem; }
            set { samplenewsitem = value; }
        }

        /// <summary>
        /// This is the Category name to filter NewsItems
        /// </summary>
        public string CategoryName
        {
            get { return categoryname; }
            set { categoryname = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string url = HttpContext.Current.Request.Url.AbsoluteUri;
                Guid taxon = GetTaxonByName(CategoryName, SampleNewsItem);
                var newsItems = App.WorkWith().NewsItems().Where(ni => ni.Status == ContentLifecycleStatus.Live && ni.Visible && !url.Contains(ni.ItemDefaultUrl)).Where(ni => ni.GetValue<TrackedList<Guid>>("Category").Contains(taxon)).OrderByDescending(i => i.FieldValue<DateTime?>("NewsDate")).Take(Count).Get();

                List<NewsExtension> ext = new List<NewsExtension>();

                foreach (NewsItem item in newsItems)
                {
                    NewsExtension extension = new NewsExtension(item);

                    if (extension != null)
                    {
                        SetItemUrl(extension);
                        ext.Add(extension);
                    }
                }

                PressReleaseSidebar.DataSource = ext;
                PressReleaseSidebar.DataBind();
            }
            catch{ }
          }

        public Guid GetTaxonByName(string taxonomyName, string releaseTitle)
        {
            Guid taxonGuid = new Guid();
            var dataItem = App.WorkWith().NewsItems().Get().Where(p => p.Title == releaseTitle).First();
            var propDesc = OrganizerBase.GetProperty(dataItem.GetType(), "Category") as TaxonomyPropertyDescriptor;
            if (propDesc != null)
            {
                var taxonIds = propDesc.GetValue(dataItem) as IList<Guid>;
                var taxonomyManager = TaxonomyManager.GetManager();
               
                foreach (var taxonId in taxonIds)
                {
                    if (taxonomyManager.GetTaxon(taxonId).Title == taxonomyName)
                    {
                        taxonGuid = taxonId;
                        return taxonGuid;
                    }
                }
            }

            return taxonGuid;
        }

        private void SetItemUrl(NewsExtension ext)
        {
            string urlCenter = "/newsroom/press-releases";
            ext.StringUrl = urlCenter + ext.ItemDefaultUrl.Value; 

        }

    }
}