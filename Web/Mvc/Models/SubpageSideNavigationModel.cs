using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Telerik.Sitefinity.Pages.Model;

namespace SitefinityWebApp.Mvc.Models
{
    public class SubpageSideNavigationModel
    {
        /// <summary>
        /// Gets or sets page properties.
        /// </summary>
        public PageNode Page { get; set; }
        public string Url { get; set; }
  
    }
}