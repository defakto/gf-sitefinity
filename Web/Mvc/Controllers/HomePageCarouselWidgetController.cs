using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Model.ContentLinks;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;
using Telerik.Sitefinity.Utilities.TypeConverters;
using Telerik.Sitefinity.RelatedData;
using System.ComponentModel;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "HomePageCarouselWidget", Title = "Home Page Carousel Widget", SectionName = "Global Foundries")]
    public class HomePageCarouselWidgetController : Controller
    {
        [Category("General")]
        public string Location { get; set; }

        public ActionResult Index()
        {
            var carouselItems = GetCarouselItems();

            return View("Default", carouselItems);
        }

        protected override void HandleUnknownAction(string actionName)
        {
            View("Default", GetCarouselItems()).ExecuteResult(this.ControllerContext);
        }

        private List<HomePageCarouselWidgetModel> GetCarouselItems()
        {
            var carouselItems = new List<HomePageCarouselWidgetModel>();

            List<DynamicContent> items = GetDynamicItems().ToList();
            items = items.Where(x => x.GetValue("Location").ToString().Contains((string.IsNullOrEmpty(Location)) ? "Home" : Location)).ToList();
            items = items.OrderBy(x => Convert.ToInt32(x.GetValue("Order"))).ToList();

            int slideNumber = 0;

            foreach (var item in items)
            {
                var carouselItem = new HomePageCarouselWidgetModel
                {
                    Title = item.GetValue("Title").ToString(),
                    MainHeading = item.GetValue("MainHeading").ToString(),
                    SubHeading = item.GetValue("SubHeading").ToString(),
                    SubSubHeading = item.GetValue("SecondSubHeading").ToString(),
                    NavigationButtonText = item.GetValue("NavigationButtonText").ToString(),
                    NavigationButtonUrl = item.GetValue("NavigationButtonUrl").ToString(),
                    SlideNumber = slideNumber,
                    CssClass = slideNumber == 0 ? "active" : "",
                    ImageUrl = GetMainImageUrl(item),                   
                    Order = Convert.ToInt32(item.GetValue("Order").ToString()),
                };

                // Set mobile image url
                carouselItem.MobileImageUrl = GetMobileImagerUrl(item, carouselItem.ImageUrl);

                carouselItems.Add(carouselItem);
                slideNumber++;
            }
            return carouselItems;
        }


        /// <summary>
        /// Returns url of mobile image. Returns main image url if no image is found
        /// </summary>
        /// <param name="item">DynamicContent carousel item</param>
        /// <param name="mainImageUrl">Url of main image</param>
        /// <returns>Mobile url if found otherwise main img url</returns>
        public string GetMobileImagerUrl(DynamicContent item, string mainImageUrl)
        {
            Image image = item.GetRelatedItems<Image>("MobileImage").FirstOrDefault();
            return (image != null) ? image.Url : mainImageUrl;
        }

        
        /// <summary>
        /// Returns url for the main image of the carousel item passed in. Returns # if no image is found
        /// </summary>
        /// <param name="item">DynamicContent carousel item</param>
        /// <returns>Image url</returns>
        public string GetMainImageUrl(DynamicContent item)
        {
            ContentLink imageContentLink = ((ContentLink[])item.GetValue("MainImage")).FirstOrDefault();
            LibrariesManager libraryManager = LibrariesManager.GetManager();
            var image = libraryManager.GetImage(imageContentLink.ChildItemId);
         
            return (image != null) ? image.Url : "#";
        }


        public IQueryable<DynamicContent> GetDynamicItems()
        {
            var carouselItems = DynamicModuleManager.GetManager()
                .GetDataItems(TypeResolutionService.ResolveType("Telerik.Sitefinity.DynamicTypes.Model.Carousel.Carouselitem"))
                .Where(cit => cit.Status == ContentLifecycleStatus.Live && cit.Visible);

            return carouselItems;
        }
    }
}