﻿using SitefinityWebApp.Custom.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;

namespace SitefinityWebApp.Mvc.Controllers
{
	[ControllerToolboxItem(Name = "ResponsiveNavigationController", Title = "Responsive Navigation Widget", SectionName = "NavigationControlsSection"), Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesigner(typeof(SitefinityWebApp.WidgetDesigners.ResponsiveNavigation.ResponsiveNavigationDesigner))]
	public class ResponsiveNavigationController : Controller
	{
		public string ControlMode { get; set; }

		public ActionResult Index()
		{
			if (string.IsNullOrEmpty(ControlMode)) ControlMode = "mobile";
			var service = new NavigationService();
			return View("Index", service.Get(ControlMode));
		}

		protected override void HandleUnknownAction(string actionName)
		{
			if (string.IsNullOrEmpty(ControlMode)) ControlMode = "mobile";
			var service = new NavigationService();
			ViewData["action"] = "Index";
			View("Index", service.Get(ControlMode)).ExecuteResult(this.ControllerContext);
		}
	}
}
