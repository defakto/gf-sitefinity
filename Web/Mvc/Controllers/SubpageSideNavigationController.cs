using System;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;
using Telerik.Sitefinity.Pages.Model;
using System.Collections.Generic;
using Telerik.Sitefinity.Modules.Pages;
using System.Globalization;
using Telerik.Sitefinity.Web;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "SubpageSideNavigation", Title = "SubpageSideNavigation", SectionName = "MvcWidgets"), Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesigner(typeof(SitefinityWebApp.WidgetDesigners.SubpageSideNavigation.SubpageSideNavigationDesigner))]
    public class SubpageSideNavigationController : Controller
    {
        /// <summary>
        /// Gets or sets the Page Id.
        /// </summary>
        [Category("Page Guid Property")]
        public Guid PageID { get; set; }

        /// <summary>
        /// This is the default Action.
        /// </summary>
        public ActionResult Index()
        {
            var model = new SubpageSideNavigationModel();
            var pages = GetSubPages();

            return View("Default", pages);
        }

        private List<SubpageSideNavigationModel> GetSubPages()
        {
            CultureInfo en = CultureInfo.GetCultureInfo("en");

            var pages = new List<SubpageSideNavigationModel>();
            PageManager pageManager = PageManager.GetManager();
            var subPages = pageManager.GetPageNodes().Where(x => x.ParentId == this.PageID).ToList();
             
           foreach (PageNode page in subPages)
           {
               if (page.ApprovalWorkflowState == "Published")
               {
                    var mPage = new SubpageSideNavigationModel();
                    mPage.Page = page;
                    string url = page.GetFullUrl(en, true);
                    url = UrlPath.ResolveUrl(url, true, true);

                    mPage.Url = url;
                    
                    pages.Add(mPage);
               }
                   
           }

            return pages;
        }
    }
}