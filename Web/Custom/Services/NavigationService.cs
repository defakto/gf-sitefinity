﻿using Babaganoush.Sitefinity.Content.Managers;
using Babaganoush.Sitefinity.Models;
using SitefinityWebApp.Custom.Enumerations;
using SitefinityWebApp.Mvc.Models;
using SitefinityWebApp.Mvc.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SitefinityWebApp.Custom.Services
{
	public class NavigationService
	{
		public NavigationViewModel Get(string controlMode)
		{
			var pages = PagesManager.Instance.GetAll();
			var currentPage = PagesManager.Instance.GetCurrentPage();

			var mainNavigationModel = new ResponsiveNavigationModel
			{
				NavigationPages = pages,
				CurrentPage = currentPage
			};

			var navigationViewModel = new NavigationViewModel { NavigationItems = mainNavigationModel, ControlMode = GetControlMode(controlMode) };
			return navigationViewModel;
		}

		public GroupedNavigationViewModel Get(int groupSize)
		{
			var pages = PagesManager.Instance.GetAll();
			var groupedPages = new List<PageModel>();
			var groupedPageList = new List<List<PageModel>>();

			int slideNumber = 0;
			bool groupAdded = false;
			foreach (var page in pages.Items)
			{
				groupAdded = false;
				groupedPages.Add(page);
				slideNumber++;

				if (slideNumber % groupSize == 0)
				{
					groupAdded = true;
					groupedPageList.Add(groupedPages);
					groupedPages = new List<PageModel>();
				}
			}
			if (!groupAdded)
			{
				groupedPageList.Add(groupedPages);
			}

			return new GroupedNavigationViewModel { GroupedPages = groupedPageList };
		}

		private ControlMode GetControlMode(string controlMode)
		{
			if (!string.IsNullOrWhiteSpace(controlMode))
			{
				controlMode = controlMode.Trim().ToLower();

				if (controlMode == "desktop")
				{
					return ControlMode.Desktop;
				}
				if (controlMode == "mobile")
				{
					return ControlMode.Mobile;
				}
			}

			return ControlMode.Desktop;
		}
	}
}
