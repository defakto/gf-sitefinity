$(function () {
    $('#mammoth-carousel').carousel();

    // swaps mobile/desktop carousel images
    (function () {
        if ($("#mammoth-carousel").length) {
            var previousView, currentView;

            $(window).on("load resize", function () {
                currentView = ($(window).width() >= 1200) ? "desktop" : "mobile";

                if (currentView != previousView) {
                    $(".carousel-inner img").each(function () {
                        $(this).attr("src", $(this).data(currentView + "-src"));
                    });

                    previousView = currentView;
                }
            });
        }

    })();
});