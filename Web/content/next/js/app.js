$(document).ready(function(){
  var nav = responsiveNav(".nav-collapse", {
    navClass: "nav-collapse",
    navActiveClass: "js-nav-active",
  });
  if($('.carousel-inner .item').is(':only-child')) {
	  $('.controls .carousel-control').hide();
  }
});

$(function() {
    $('#mammoth-carousel').carousel();
	
	 if($('.carousel-inner .item').length === 1) {
	  $('.carousel-control').hide();
  }
});